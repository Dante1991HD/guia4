#include <iostream>
#include <unistd.h>
#include <fstream>
#include "Arbol.h"

// Funcion del menú
void menu(){
    // Se imprimen las opciones del usuario
    cout << endl;
    cout << "---------------------------------------------------" << endl;
    cout << "Ingrese una de las siguientes opciones" << endl;
    cout << "[1] Insertar numero al arbol" << endl;
    cout << "[2] Eliminar numero del arbol" << endl;
    cout << "[3] Modificar un elemento del arbol" << endl;
    cout << "[4] Mostrar contenido del arbol " << endl;
    cout << "[5] Generar el grafo" << endl;
    cout << "[6] Salir del programa" << endl;
    cout << "---------------------------------------------------" << endl;
    cout << "¿Que desea hacer?  ";

}

// Funcion principal
int main(int argc, char* argv[]){
    // Variables de apoyo para el codigo
    bool on = true;
    string line, opcion;
    int eliminar = 0;
    int numero = 1;
    // Creamos un nodo para utilizarlo para capturar la raiz
    Nodo *raiz = new Nodo();
    // Y creamos nuestro arbol binario
    Arbol *arbol = new Arbol();

    // Ciclo principal del programa
    while(on){
        menu();
        getline(cin, opcion);
        system("clear");
        switch (stoi(opcion))
        {
        case 1:
            cout << "Numero a agregar: ";
            getline(cin, line);
            numero = stoi(line);
            arbol->crear_nodo(numero);
            break;
        case 2:
            raiz = arbol->get_raiz();
            // Nos aseguramos que el arbol no se encuentre vacio
            if(raiz != NULL){
                // Mostramos los elementos que contiene el arbol
                cout << "A continuación se mostrarán los nodos del arbol para elegir el que se desea eliminar" << endl;
                arbol->inorden(raiz);
                cout << endl;
                // Pedimos el dato a eliminar
                cout << "Ingrese el numero a eliminar: "; 
                getline(cin, line);
                eliminar = stoi(line);
                arbol->eliminar_nodo(arbol->get_raiz(), eliminar);
                // Se llama la funcion de busqueda del nodo que despues deriva en la eliminacion
                eliminar = 0;
            }
            else{
                cout << "El arbol se encuentra vacío" << endl << endl;
            }
            break;
        case 3:
            raiz = arbol->get_raiz();
            // Nos aseguramos que el arbol no se encuentre vacio
            if(raiz != NULL){
                // Mostramos el arbol como se encuentra actualmente
                cout << "A continuación se mostrarán los nodos del arbol para elegir el que se desea eliminar" << endl;
                arbol->inorden(raiz);
                cout << endl;
                // Pedimos el numero a editar y el numero que se actualizara en esa posicion
                cout << "Ingrese el numero a editar: "; 
                getline(cin, line);
                arbol->eliminar_nodo(arbol->get_raiz(), stoi(line));
                cout << "Ingrese el numero que querrá actualizar en esa posicion: "; 
                getline(cin, line);
                numero = stoi(line);
                arbol->crear_nodo(numero);
                cout << "Se ha generado el cambio que ud necesitaba" << endl;
            }
            else{
                cout << "El arbol se encuentra vacio" << endl;
            }
            break;
        case 4:
            raiz = arbol->get_raiz();
            // Nos aseguraremos de que no este vacio
            if(raiz != NULL){
                // Se mostraran todas las posibles de recorrido del arbol
                cout << "-------- ARBOL ACTUAL --------" << endl << endl;
                cout << "----- METODO INORDEN -----" << endl;
                arbol->inorden(raiz);
                cout << endl;
                cout << "----- METODO POSTORDEN -----" << endl;
                arbol->postorden(raiz);
                cout << endl;
                cout << "----- METODO PREORDEN -----" << endl;
                arbol->preorden(raiz);
                cout << endl;
            }
            else{
                cout << "El arbol se encuentra vacio" << endl;
            }
            break;
        case 5:
            // Capturamos la raiz del arbol
            raiz = arbol->get_raiz();
            // Si el arbol no se encuentra vacio crearemos un grafico
            if(raiz != NULL){
                // Se llama al metodo de la clase arbol para crear el grafo
                arbol->crear_grafico(raiz);
            }
            // Sino solo se pasa
            else{
                cout << "El arbol se encuentra vacio" << endl;
            }
            break;
        case 6:
            cout << "Gracias por utilizar el programa" << endl;
            on = false;
            break;
        default:
            cout << "Ingrese una opcion valida" << endl;
            break;
        }
    }
        delete(raiz);
        delete(arbol);
    return 0;
}
