# Guía IV UII

## Descripción del programa 
El programa permite la generación de un árbol binario de búsqueda, posee metodos para: inserción de elementos al árbol, eliminación, modificación, presenta los datos en pantalla en tres ordenes distintos (Inorden, postorden, preorden), y ademas puede generar una imagen representativa de las relaciones establecidas en el árbol.

## Requerimientos para utilizar el programa
* Sistema operativo Linux
* Compilador make
* Software Graphviz
En caso de no contar con make:

### ¿Como instalar make?
Para instalar make, Ud. debe acceder a una terminal dentro del sistema operativo linux e ingresar el siguiente comando:
```
sudo apt-get install make
```
Luego ingresar clave de usuario el sistema procederá a descargar e instalar el compilador. Una vez termine la descarga, su compilador estará listo para usar.
En el caso de no contar con Graphviz
### ¿Como instalar graphviz?
Para instalar graphviz, Ud. debe acceder a una terminal dentro del sistema operativo linux e ingresar el siguiente comando:
```
sudo apt-get install graphviz
```
Luego ingresar clave de usuario y el sistema procederá a descargar e instalar el paquete. Una vez termine la descarga, el paquete estará listo para usar

## Compilación y ejecución del programa
* Abrir una terminal en el directorio donde se ubican los archivos del programa e ingresar el siguiente comando.
```
make
```
* Luego de completada la compilación utilice el siguiente comando.
```
./programa
```

## Autor
* Dante Aguirre
* Correo: daguirre12@alumnos.utalca.cl

