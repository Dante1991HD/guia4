#include <iostream>
#include <unistd.h>
#include <fstream>
#include "Arbol.h"

using namespace std;

// Constructor por defecto de la clase arbol
Arbol::Arbol() {}

// Metodo para obtener raiz del arbol
Nodo * Arbol::get_raiz() {
    return this -> raiz;
}

//Metodo para crear nodo
void Arbol::crear_nodo(int numero) {
    Nodo * tmp = new Nodo;
    // Se asigna la info al nodo que se desea guardar
    tmp -> numero = numero;
    // Por defecto, la izquierda y la derecha apuntarán a nulo
    tmp -> izq = NULL;
    tmp -> der = NULL;

    // Ahora insertamos el nodo a la raíz
    this -> raiz = insertar_nodo(this -> raiz, tmp);
}

// Metodo para buscar el nodo lo que nos entregará un true o un false
void Arbol::buscar_nodo(Nodo * tmp, int eliminar) {
    // Se empezará a buscar el nodo
    // Si el nodo es menor por la izquierda, se busca por izq
    if (eliminar < tmp -> numero) {
        // Si el nodo está apuntando a nulo es porque no existe el nodo
        if (tmp -> izq == NULL) {
            cout << "No está el numero" << endl;
        } else {
            // Si no es nulo es porque quizás existe el nodo por lo tanto
            // se debe seguir buscando por izquierda
            buscar_nodo(tmp -> izq, eliminar);
        }
    }
    // Si el numero no es menor por la izquierda, será mayor por derecha
    else {
        // Nos aseguraremos de que el numero sea mayor y se busque
        if (eliminar > tmp -> numero) {
            // Seguimos el mismo procedimiento que por izquierda
            if (tmp -> der == NULL) {
                cout << "No está el numero" << endl;
            } else {
                buscar_nodo(tmp -> der, eliminar);
            }
        } // Pero si no es ni mayor por derecha ni por izquierda nos da a pensar que es el mismo numero
        else {
            cout << "Se encontró el numero a eliminar" << endl;
            // this->raiz = eliminar_nodo(this->raiz, eliminar);
        }
    }
}
// Metodo para insertar nodo
Nodo * Arbol::insertar_nodo(Nodo * tmp, Nodo * nuevo) {
    // Si el nodo tmp está vacio se debe agregar el nodo en esa posición
    if (tmp == NULL) {
        tmp = nuevo;
        cout << "Se agregó el dato con exito" << endl << endl;
    }
    // Si es que no está vacio
    else {
        // Vemos si el valor es menor al de la raiz 
        if (nuevo -> numero < tmp -> numero) {
            tmp -> izq = insertar_nodo(tmp -> izq, nuevo);
        }
        // Sino se agrega por la derecha
        else if (nuevo -> numero > tmp -> numero) {
            tmp -> der = insertar_nodo(tmp -> der, nuevo);
        }
        // Osino son iguales y no se pueden agregar datos repetidos
        else {
            cout << "Numero repetido, ingresar un nuevo numero" << endl << endl;
        }
    }
    return tmp;
}
// Metodo para eliminar un nodo
void Arbol::eliminar_nodo(Nodo * tmp, int numero) {
    Nodo * aux1;
    if (tmp != NULL) {
        if (numero < tmp -> numero) {
            eliminar_nodo(tmp -> izq, numero);
        } else {
            if (numero > tmp -> numero) {
                eliminar_nodo(tmp -> der, numero);
            } else {
                Nodo * otro = tmp;
                if (otro -> der == NULL) {
                    tmp = otro -> izq;
                } else {
                    if (otro -> izq == NULL) {
                        tmp = otro -> der;
                    } else {
                        Nodo * aux = tmp -> izq;
                        bool ban = false;
                        while (aux -> der != NULL) {
                            aux1 = aux;
                            aux = aux -> der;
                            ban = true;
                        }
                        tmp -> numero = aux -> numero;
                        otro = aux;
                        if (ban == true) {
                            aux1 -> der = aux -> izq;
                        } else {
                            tmp -> izq = aux -> izq;
                        }
                    }
                }
                delete otro;
            }
        }

    } else {
        cout << "Información a eliminar no se encuentra en el árbol" << endl;
    }
}

// Imprimir el arbol en preorden 
void Arbol::preorden(Nodo * tmp) {
    if (tmp != NULL) {
        cout << "[" << tmp -> numero << "]" << "->";
        preorden(tmp -> izq);
        preorden(tmp -> der);
    }
}

// Imprimir el arbol en inorden
void Arbol::inorden(Nodo * tmp) {
    if (tmp != NULL) {
        inorden(tmp -> izq);
        cout << "[" << tmp -> numero << "]" << "->";
        inorden(tmp -> der);
    }
}

// Imprimir el arbol en postorden
void Arbol::postorden(Nodo * tmp) {
    if (tmp != NULL) {
        postorden(tmp -> izq);
        postorden(tmp -> der);
        cout << "[" << tmp -> numero << "]" << "->";
    }
}

// Metodo para crear el grafico del arbol
void Arbol::crear_grafico(Nodo * tmp) {
    //creacion de archivo.txt
    ofstream archivo("grafo.txt", ios::out);
    // Primero debemos intentar abrir el archivo para evitar errores
    if (archivo.fail()) {
        cout << "**No se pudo abrir archivo**" << endl;
    } else {
        // Escritura en el archivo del arbol
        archivo << "digraph G {\n";
        archivo << "node [style=filled fillcolor=red] ;\n";
        // llamada a metodo para ingresar informacion del arbol
        recorrer_arbol(tmp, archivo);
        archivo << "}";
        archivo.close();
        // Generacion de la imagen a traves del archivo de texto
        system("dot -Tpng -ografo.png grafo.txt &");
        // Apertura de la imagen
        system("eog grafo.png &");
    }
}

// Funcion que recorre el arbol y crea el archivo.txt
void Arbol::recorrer_arbol(Nodo * tmp, ofstream & archivo) {
    // Recorrido del arbol en preorden y se agregan datos al archivo.txt
    if (tmp != NULL) {
        if (tmp -> izq != NULL) {
            archivo << tmp -> numero << "->" << tmp -> izq -> numero << ";\n";
        } else {
            archivo << '"' << tmp -> numero << "i" << '"' << " [shape=point];\n";
            archivo << tmp -> numero << "->" << '"' << tmp -> numero << "i" << '"' << ";\n";
        }
        if (tmp -> der != NULL) {
            archivo << tmp -> numero << "->" << tmp -> der -> numero << ";\n";
        } else {
            archivo << '"' << tmp -> numero << "d" << '"' << " [shape=point];\n";
            archivo << tmp -> numero << "->" << '"' << tmp -> numero << "d" << '"' << ";\n";
        }
        // Luego se recorre en izquierda y derecha
        recorrer_arbol(tmp -> izq, archivo);
        recorrer_arbol(tmp -> der, archivo);
    }
};