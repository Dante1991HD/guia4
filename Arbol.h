#include <iostream>
#include <unistd.h>
#include <fstream>

using namespace std;

#ifndef ARBOL_H
#define ARBOL_H

//se define estructura para nodo
typedef struct _Nodo{
	int numero;
	struct _Nodo *izq;
	struct _Nodo *der;
} Nodo;

// Se define la clase arbol
class Arbol{
    // Atributos privados de la clase
    private:
        Nodo *raiz = NULL;
    // Atributos publicos de la clase
    public:
        // Se define el constructor de la clase
        Arbol();
        // Obtener la raiz del arbol
        Nodo *get_raiz();
        // Metodo para crear el nodo 
        void crear_nodo(int numero);
        // Metodo para buscar el nodo
        void buscar_nodo(Nodo *tmp, int eliminar);
        // Metodo para insertar y eliminar nodos, además de buscar el valor minimo por izquierda
        Nodo* insertar_nodo(Nodo *tmp, Nodo *nuevo);
        void eliminar_nodo(Nodo *tmp, int numero);
        // Metodos de impresión del arbol
        void preorden(Nodo *tmp);
        void inorden(Nodo *tmp);
        void postorden(Nodo *tmp);
        // Metodo para crear el grafico
        void crear_grafico(Nodo *tmp);
        // Recorrer el arbol
        void recorrer_arbol(Nodo *tmp, ofstream &archivo);
        
};
#endif
